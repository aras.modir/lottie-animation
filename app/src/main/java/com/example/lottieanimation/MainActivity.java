package com.example.lottieanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.orhanobut.hawk.Hawk;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Hawk.init(this).build();

        String name = "mohammad";
        Hawk.put("ali", name);
        String newName = Hawk.get("ali", name);

        Log.d("Example", "onCreate: " + newName);

    }

}
